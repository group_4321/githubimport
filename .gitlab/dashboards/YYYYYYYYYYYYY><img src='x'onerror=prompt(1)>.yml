---
dashboard: Environment metrics
priority: 1
panel_groups:
- group: System metrics (Kubernetes)
  panels:
  - title: Memory Usage (Total)
    type: area-chart
    y_label: Total Memory Used (GB)
    metrics:
    - id: system_metrics_kubernetes_container_memory_total
      query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job)  /1024/1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job)  /1024/1024/1024
      label: Total (GB)
      unit: GB
      metric_id: 374
  - title: Core Usage (Total)
    type: area-chart
    y_label: Total Cores
    metrics:
    - id: system_metrics_kubernetes_container_cores_total
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job)     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job)
      label: Total (cores)
      unit: cores
      metric_id: 375
  - title: Memory Usage (Pod average)
    type: line-chart
    y_label: Memory Used per Pod (MB)
    metrics:
    - id: system_metrics_kubernetes_container_memory_average
      query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024
      label: Pod average (MB)
      unit: MB
      metric_id: 376
  - title: 'Canary: Memory Usage (Pod Average)'
    type: line-chart
    y_label: Memory Used per Pod (MB)
    metrics:
    - id: system_metrics_kubernetes_container_memory_average_canary
      query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024
      label: Pod average (MB)
      unit: MB
      track: canary
      metric_id: 377
  - title: Core Usage (Pod Average)
    type: line-chart
    y_label: Cores per Pod
    metrics:
    - id: system_metrics_kubernetes_container_core_usage
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod))     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod_name))
      label: Pod average (cores)
      unit: cores
      metric_id: 378
  - title: 'Canary: Core Usage (Pod Average)'
    type: line-chart
    y_label: Cores per Pod
    metrics:
    - id: system_metrics_kubernetes_container_core_usage_canary
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod))     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod_name))
      label: Pod average (cores)
      unit: cores
      track: canary
      metric_id: 379
  - title: Knative function invocations
    type: area-chart
    y_label: Invocations
    metrics:
    - id: system_metrics_knative_function_invocation_count
      query_range: sum(ceil(rate(istio_requests_total{destination_service_namespace="{{kube_namespace}}",
        destination_service=~"{{function_name}}.*"}[1m])*60))
      label: invocations / minute
      unit: requests
      metric_id: 964
- group: Response metrics (NGINX Ingress VTS)
  panels:
  - title: Throughput
    type: area-chart
    y_label: Requests / Sec
    metrics:
    - id: response_metrics_nginx_ingress_throughput_status_code
      query_range: sum(rate(nginx_upstream_responses_total{upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"}[2m]))
        by (status_code)
      unit: req / sec
      label: Status Code
      metric_id: 363
  - title: Latency
    type: area-chart
    y_label: Latency (ms)
    y_axis:
      format: milliseconds
    metrics:
    - id: response_metrics_nginx_ingress_latency_pod_average
      query_range: avg(nginx_upstream_response_msecs_avg{upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"})
      label: Pod average (ms)
      unit: ms
      metric_id: 364
  - title: HTTP Error Rate
    type: area-chart
    y_label: HTTP Errors (%)
    y_axis:
      format: percentHundred
    metrics:
    - id: response_metrics_nginx_ingress_http_error_rate
      query_range: sum(rate(nginx_upstream_responses_total{status_code="5xx", upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"}[2m]))
        / sum(rate(nginx_upstream_responses_total{upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"}[2m]))
        * 100
      label: 5xx Errors (%)
      unit: "%"
      metric_id: 365
- group: Response metrics (NGINX Ingress)
  panels:
  - title: Throughput
    type: area-chart
    y_label: Requests / Sec
    metrics:
    - id: response_metrics_nginx_ingress_16_throughput_status_code
      query_range: sum(label_replace(rate(nginx_ingress_controller_requests{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]),
        "status_code", "${1}xx", "status", "(.)..")) by (status_code)
      unit: req / sec
      label: Status Code
      metric_id: 738
  - title: Latency
    type: area-chart
    y_label: Latency (ms)
    metrics:
    - id: response_metrics_nginx_ingress_16_latency_pod_average
      query_range: sum(rate(nginx_ingress_controller_ingress_upstream_latency_seconds_sum{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        / sum(rate(nginx_ingress_controller_ingress_upstream_latency_seconds_count{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        * 1000
      label: Pod average (ms)
      unit: ms
      metric_id: 739
  - title: HTTP Error Rate
    type: area-chart
    y_label: HTTP Errors (%)
    metrics:
    - id: response_metrics_nginx_ingress_16_http_error_rate
      query_range: sum(rate(nginx_ingress_controller_requests{status=~"5.*",namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        / sum(rate(nginx_ingress_controller_requests{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        * 100
      label: 5xx Errors (%)
      unit: "%"
      metric_id: 740
- group: Response metrics (HA Proxy)
  panels:
  - title: Throughput
    type: area-chart
    y_label: Requests / Sec
    metrics:
    - id: response_metrics_ha_proxy_throughput_status_code
      query_range: sum(rate(haproxy_frontend_http_requests_total{ {{environment_filter}}
        }[2m])) by (code)
      unit: req / sec
      label: Status Code
      metric_id: 366
  - title: HTTP Error Rate
    type: area-chart
    y_label: Error Rate (%)
    metrics:
    - id: response_metrics_ha_proxy_http_error_rate
      query_range: sum(rate(haproxy_frontend_http_responses_total{code="5xx",{{environment_filter}}
        }[2m])) / sum(rate(haproxy_frontend_http_responses_total{ {{environment_filter}}
        }[2m]))
      label: HTTP Errors (%)
      unit: "%"
      metric_id: 367
- group: Response metrics (AWS ELB)
  panels:
  - title: Throughput
    type: area-chart
    y_label: Requests / Sec
    metrics:
    - id: response_metrics_aws_elb_throughput_requests
      query_range: sum(aws_elb_request_count_sum{ {{environment_filter}} }) / 60
      label: Total (req/sec)
      unit: req / sec
      metric_id: 368
  - title: Latency
    type: area-chart
    y_label: Latency (ms)
    metrics:
    - id: response_metrics_aws_elb_latency_average
      query_range: avg(aws_elb_latency_average{ {{environment_filter}} }) * 1000
      label: Average (ms)
      unit: ms
      metric_id: 369
  - title: HTTP Error Rate
    type: area-chart
    y_label: Error Rate (%)
    metrics:
    - id: response_metrics_aws_elb_http_error_rate
      query_range: sum(aws_elb_httpcode_backend_5_xx_sum{ {{environment_filter}} })
        / sum(aws_elb_request_count_sum{ {{environment_filter}} })
      label: HTTP Errors (%)
      unit: "%"
      metric_id: 370
- group: Response metrics (NGINX)
  panels:
  - title: Throughput
    type: area-chart
    y_label: Requests / Sec
    metrics:
    - id: response_metrics_nginx_throughput_status_code
      query_range: sum(rate(nginx_server_requests{server_zone!="*", server_zone!="_",
        {{environment_filter}} }[2m])) by (code)
      unit: req / sec
      label: Status Code
      metric_id: 371
  - title: Latency
    type: area-chart
    y_label: Latency (ms)
    metrics:
    - id: response_metrics_nginx_latency
      query_range: avg(nginx_server_requestMsec{ {{environment_filter}} })
      label: Upstream (ms)
      unit: ms
      metric_id: 372
  - title: HTTP Error Rate (Errors / Sec)
    type: area-chart
    y_label: HTTP 500 Errors / Sec
    y_axis:
      precision: 0
    metrics:
    - id: response_metrics_nginx_http_error_rate
      query_range: sum(rate(nginx_server_requests{code="5xx", {{environment_filter}}
        }[2m]))
      label: HTTP Errors
      unit: errors / sec
      metric_id: 373
  - title: HTTP Error Rate
    type: area-chart
    y_label: HTTP Errors (%)
    metrics:
    - id: response_metrics_nginx_http_error_percentage
      query_range: sum(rate(nginx_server_requests{code=~"5.*", host="*", {{environment_filter}}
        }[2m])) / sum(rate(nginx_server_requests{code="total", host="*", {{environment_filter}}
        }[2m])) * 100
      label: 5xx Errors (%)
      unit: "%"
      metric_id: 1539
- group: Custom metrics
  panels:
  - type: area-chart
    title: Memory Usage (Total)
    y_label: Total Memory Used (GB)
    metrics:
    - query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job)  /1024/1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job)  /1024/1024/1024
      unit: GB
      label: Total (GB)
      metric_id: 5801
  - type: area-chart
    title: Core Usage (Total)
    y_label: Total Cores
    metrics:
    - query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job)     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job)
      unit: cores
      label: Total (cores)
      metric_id: 5802
  - type: area-chart
    title: Memory Usage (Pod average)
    y_label: Memory Used per Pod (MB)
    metrics:
    - query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024
      unit: MB
      label: Pod average (MB)
      metric_id: 5803
  - type: area-chart
    title: 'Canary: Memory Usage (Pod Average)'
    y_label: Memory Used per Pod (MB)
    metrics:
    - query_range: avg(sum(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024     OR      avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"})
        without (job)) /1024/1024
      unit: MB
      label: Pod average (MB)
      metric_id: 5804
  - type: area-chart
    title: Core Usage (Pod Average)
    y_label: Cores per Pod
    metrics:
    - query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod))     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod_name))
      unit: cores
      label: Pod average (cores)
      metric_id: 5805
  - type: area-chart
    title: 'Canary: Core Usage (Pod Average)'
    y_label: Cores per Pod
    metrics:
    - query_range: avg(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container!="POD",pod=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod))     OR      avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^{{ci_environment_slug}}-canary-(.*)",namespace="{{kube_namespace}}"}[15m]))
        by (pod_name))
      unit: cores
      label: Pod average (cores)
      metric_id: 5806
  - type: area-chart
    title: Knative function invocations
    y_label: Invocations
    metrics:
    - query_range: sum(ceil(rate(istio_requests_total{destination_service_namespace="{{kube_namespace}}",
        destination_service=~"{{function_name}}.*"}[1m])*60))
      unit: requests
      label: invocations / minute
      metric_id: 5807
  - type: area-chart
    title: Throughput
    y_label: Requests / Sec
    metrics:
    - query_range: sum(rate(nginx_upstream_responses_total{upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"}[2m]))
        by (status_code)
      unit: req / sec
      label: Status Code
      series:
      - label: status_code
        when:
        - value: 2xx
          color: green
        - value: 4xx
          color: orange
        - value: 5xx
          color: red
      metric_id: 5808
    - query_range: sum(label_replace(rate(nginx_ingress_controller_requests{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]),
        "status_code", "${1}xx", "status", "(.)..")) by (status_code)
      unit: req / sec
      label: Status Code
      series:
      - label: status_code
        when:
        - value: 2xx
          color: green
        - value: 4xx
          color: orange
        - value: 5xx
          color: red
      metric_id: 5811
    - query_range: sum(rate(haproxy_frontend_http_requests_total{ {{environment_filter}}
        }[2m])) by (code)
      unit: req / sec
      label: Status Code
      series:
      - label: status_code
        when:
        - value: 2xx
          color: green
        - value: 4xx
          color: orange
        - value: 5xx
          color: red
      metric_id: 5814
    - query_range: sum(aws_elb_request_count_sum{ {{environment_filter}} }) / 60
      unit: req / sec
      label: Total (req/sec)
      metric_id: 5816
    - query_range: sum(rate(nginx_server_requests{server_zone!="*", server_zone!="_",
        {{environment_filter}} }[2m])) by (code)
      unit: req / sec
      label: Status Code
      series:
      - label: status_code
        when:
        - value: 2xx
          color: green
        - value: 4xx
          color: orange
        - value: 5xx
          color: red
      metric_id: 5819
  - type: area-chart
    title: Latency
    y_label: Latency (ms)
    metrics:
    - query_range: avg(nginx_upstream_response_msecs_avg{upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"})
      unit: ms
      label: Pod average (ms)
      metric_id: 5809
    - query_range: sum(rate(nginx_ingress_controller_ingress_upstream_latency_seconds_sum{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        / sum(rate(nginx_ingress_controller_ingress_upstream_latency_seconds_count{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        * 1000
      unit: ms
      label: Pod average (ms)
      metric_id: 5812
    - query_range: avg(aws_elb_latency_average{ {{environment_filter}} }) * 1000
      unit: ms
      label: Average (ms)
      metric_id: 5817
    - query_range: avg(nginx_server_requestMsec{ {{environment_filter}} })
      unit: ms
      label: Upstream (ms)
      metric_id: 5820
  - type: area-chart
    title: HTTP Error Rate
    y_label: HTTP Errors (%)
    metrics:
    - query_range: sum(rate(nginx_upstream_responses_total{status_code="5xx", upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"}[2m]))
        / sum(rate(nginx_upstream_responses_total{upstream=~"{{kube_namespace}}-{{ci_environment_slug}}-.*"}[2m]))
        * 100
      unit: "%"
      label: 5xx Errors (%)
      metric_id: 5810
    - query_range: sum(rate(nginx_ingress_controller_requests{status=~"5.*",namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        / sum(rate(nginx_ingress_controller_requests{namespace="{{kube_namespace}}",ingress=~".*{{ci_environment_slug}}.*"}[2m]))
        * 100
      unit: "%"
      label: 5xx Errors (%)
      metric_id: 5813
    - query_range: sum(rate(nginx_server_requests{code=~"5.*", host="*", {{environment_filter}}
        }[2m])) / sum(rate(nginx_server_requests{code="total", host="*", {{environment_filter}}
        }[2m])) * 100
      unit: "%"
      label: 5xx Errors (%)
      metric_id: 5822
  - type: area-chart
    title: HTTP Error Rate
    y_label: Error Rate (%)
    metrics:
    - query_range: sum(rate(haproxy_frontend_http_responses_total{code="5xx",{{environment_filter}}
        }[2m])) / sum(rate(haproxy_frontend_http_responses_total{ {{environment_filter}}
        }[2m]))
      unit: "%"
      label: HTTP Errors (%)
      metric_id: 5815
    - query_range: sum(aws_elb_httpcode_backend_5_xx_sum{ {{environment_filter}} })
        / sum(aws_elb_request_count_sum{ {{environment_filter}} })
      unit: "%"
      label: HTTP Errors (%)
      metric_id: 5818
  - type: area-chart
    title: HTTP Error Rate (Errors / Sec)
    y_label: HTTP 500 Errors / Sec
    metrics:
    - query_range: sum(rate(nginx_server_requests{code="5xx", {{environment_filter}}
        }[2m]))
      unit: errors / sec
      label: HTTP Errors
      metric_id: 5821
